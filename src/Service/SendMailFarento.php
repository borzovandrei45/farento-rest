<?php

namespace App\Service;

use App\Entity\User;
use Twig\Environment;

/**
 * Class SendMailFarento
 *
 * @package App\Service
 */
class SendMailFarento
{
    private $mailer;

    private $twig;

    private $mailFromAddress;

    /**
     * @param \Swift_Mailer $mailer
     * @param Environment $twig
     * @param string $mailFromAddress
     */
    public function __construct(\Swift_Mailer $mailer, Environment $twig, string $mailFromAddress)
    {
        $this->mailer          = $mailer;
        $this->twig            = $twig;
        $this->mailFromAddress = $mailFromAddress;
    }

    /**
     * @param User $user
     *
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function sendEmail(User $user, string $password)
    {
        $email = (new \Swift_Message('Farento'))
            ->setFrom($this->mailFromAddress)
            ->setTo($user->getEmail())
            ->setBody(
                $this->twig->render(
                    'EmailTemplates/email.html.twig',
                    [
                        'username' => $user->getUsername(),
                        'password' => $password,
                    ]
                )
            );

        $this->mailer->send($email);
    }

}