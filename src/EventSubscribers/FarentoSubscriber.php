<?php


namespace App\EventSubscribers;

use App\Event\RegisterEvent;
use App\Service\SendMailFarento;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class FarentoSubscriber implements EventSubscriberInterface
{
    /** @var SendMailFarento */
    private $farentoMailer;

    /**
     * FarentoSubscriber constructor.
     *
     * @param SendMailFarento $farentoMailer
     */
    public function __construct(SendMailFarento $farentoMailer)
    {
        $this->farentoMailer = $farentoMailer;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            RegisterEvent::class => 'sendEmail',
        ];
    }

    /**
     * @param RegisterEvent $event
     *
     * @return void
     *
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function sendEmail(RegisterEvent $event): void
    {
        $this->farentoMailer->sendEmail($event->getUser(), $event->getPassword());
    }
}