<?php

namespace App\Controller;

use App\Entity\User;
use App\Event\RegisterEvent;
use App\Form\RegistrationType;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class SecurityController
 *
 * @package App\Controller
 */
class PublicController extends AbstractFOSRestController
{
    /** @var UserPasswordEncoderInterface */
    private $passwordEncoder;

    /** @var EventDispatcherInterface  */
    private $dispatcher;

    /** @var UserManagerInterface */
    private $userManager;

    /**
     * SecurityController constructor.
     *
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param EventDispatcherInterface     $dispatcher
     * @param UserManagerInterface         $userManager
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder, EventDispatcherInterface $dispatcher, UserManagerInterface $userManager)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->dispatcher      = $dispatcher;
        $this->userManager     = $userManager;
    }

    /**
     * @FOSRest\Route
     * @FOSRest\View
     *
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function postRegAction(Request $request)
    {
        $form = $this->createForm(RegistrationType::class)->handleRequest($request);

        if (!$form->isValid()){
            return new JsonResponse('bad', Response::HTTP_BAD_REQUEST);
        }
        /** @var User $user */
        $user = $this->userManager->createUser();

        $email         = $form->get('email')->getData();
        $plainPassword = \base64_encode(\random_bytes(5));
        $password      = $this->passwordEncoder->encodePassword($user, $plainPassword);
        $user
            ->setUsername(\stristr($email, '@', true))
            ->setPassword($password)
            ->setEmail($email);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->flush();

        $event = new RegisterEvent($user, $plainPassword);
        $this->dispatcher->dispatch($event);

        return new JsonResponse('ok');
    }

}