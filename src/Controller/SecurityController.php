<?php

namespace App\Controller;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use JMS\Serializer\Serializer;
use JMS\Serializer\SerializerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
/**
 * Class SecurityController
 *
 * @package App\Controller
 */
class SecurityController extends AbstractFOSRestController
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;
    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * SecurityController constructor.
     *
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param SerializerInterface $serializer
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder, SerializerInterface $serializer)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->serializer      = $serializer;
    }

    /**
     * @FOSRest\Route
     * @FOSRest\View
     *
     * @IsGranted("ROLE_FARENTO", statusCode=403, message="Test Access Denied.")
     *
     * @param Request $request
     *
     * @return array
     */
    public function getContentAction(Request $request)
    {
        return [$this->getUser()];
    }
}