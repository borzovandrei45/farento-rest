<?php


namespace App\Event;


use App\Entity\User;
use Symfony\Contracts\EventDispatcher\Event;

class RegisterEvent extends Event
{
    public const NAME = 'send.email.registration';

    /** @var User */
    protected $user;

    /** @var string */
    protected $password;

    public function __construct(User $user, string $password)
    {
        $this->user     = $user;
        $this->password = $password;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function getPassword()
    {
        return $this->password;
    }
}