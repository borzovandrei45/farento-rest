<?php

namespace App\Service;

use App\Entity\User;
use App\Tests\RestTestCase;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\Mime\Email;
use Twig\Environment;

/**
 * Class SendMailFarento
 *
 * @package App\Service
 */
class SendMailFarentoTest extends RestTestCase
{
    private $testMailer;

    private $testRandomPassword = 'testRandomPassword';

    /** @var User $user */
    private $user;

    public function setUp()
    {
        parent::setUp();

        /** @var Environment $twig */
        $twig            = self::$container->get(Environment::class);
        $mailFromAddress = 'test@email';

        $this->user = $this->em->getRepository(User::class)->findAll()[0];

        /** @var \Swift_Mailer|MockObject $mailer */
        $mailer = $this->createMock(\Swift_Mailer::class);

        $mailer->expects(self::once())
            ->method('send')
            ->willReturnCallback(function ($mail) use ($mailFromAddress){
                /** @var Email $mail */
                $this->assertContains('Hi <p id="username-id">' . $this->user->getUsername() . '</p>! You\'re successfully registered.', $mail->getBody());
                $this->assertContains($this->testRandomPassword, $mail->getBody());
                $this->assertArrayHasKey($mailFromAddress, $mail->getFrom());
                $this->assertNotNull($mail->getTo());
            });

        $this->testMailer = new SendMailFarento($mailer, $twig, $mailFromAddress);
    }

    public function testMail()
    {
        $this->testMailer->sendEmail($this->user, $this->testRandomPassword);
    }

}