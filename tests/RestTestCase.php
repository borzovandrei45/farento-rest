<?php


namespace App\Tests;

use Doctrine\ORM\EntityManagerInterface;
use OAuth2\OAuth2;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;

abstract class RestTestCase extends WebTestCase
{
    /** @var KernelBrowser */
    protected $client;

    /** @var RouterInterface */
    protected $router;

    /** @var EntityManagerInterface */
    protected $em;

    public function setUp()
    {
        $this->client = self::createClient();
        $this->client->disableReboot();

        $container    = self::$container;
        $this->router = $container->get(RouterInterface::class);
        $this->em     = $container->get('doctrine.orm.default_entity_manager');

        $this->em->beginTransaction();
    }

    public function tearDown()
    {
        $this->em->rollback();
    }

    protected function loginAndGetToken(): string
    {
        $url = $this->router->generate('fos_oauth_server_token');

        $this->client->request(Request::METHOD_POST, $url, $this->getParams());
        $response = $this->client->getResponse();
        $content  = \json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        return $content['access_token'];
    }

    /**
     *  for client_id, client_secret you need create client (fos-client-bundle)
     *  for username, password you need create user (fos-user-bundle)
     */
    private function getParams(): array
    {
        return [
            'client_id'     => '2_djkhrnmf19ssgccwwc404s4408c4scooso40w8csk4488c844',
            'client_secret' => '1t1jebtrbvdw8s08c48cg8gw0gscw08s08oc4swggs0c0oggk',
            'response_type' => OAuth2::RESPONSE_TYPE_ACCESS_TOKEN,
            'grant_type'    => OAuth2::GRANT_TYPE_USER_CREDENTIALS,
            'username'      => 'test',
            'password'      => 'test',
        ];
    }

}