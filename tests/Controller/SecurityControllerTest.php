<?php


namespace App\Tests\rest_api\Controller;

use App\Entity\User;
use App\Security\TokenAuthenticator;
use App\Tests\RestTestCase;
use JMS\Serializer\Serializer;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * Class SecurityControllerTest
 *
 * @package App\Tests\rest_api\Controller
 */
class SecurityControllerTest extends RestTestCase
{
    /** @var TokenAuthenticator $tokenAuth */
    private $tokenAuth;

    /** @var UserProviderInterface $userProvider */
    private $userProvider;


    /** @var Serializer $serializer */
    private $serializer;

    private const ROLE = 'ROLE_FARENTO';

    public function setUp()
    {
        parent::setUp();

        $this->tokenAuth    = self::$container->get(TokenAuthenticator::class);
        $this->userProvider = self::$container->get(UserProviderInterface::class);
        $this->serializer   = self::$container->get(SerializerInterface::class);
    }

    public function testGetContent()
    {
        $token = $this->loginAndGetToken();

        $url = $this->router->generate('get_content');

        $this->client->request(Request::METHOD_GET, $url, [], [], [
            'HTTP_AUTHORIZATION' => $token,
        ]);
        $response = $this->client->getResponse();
        $content = \json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
        $this->assertEquals('Test Access Denied.', $content['error']['exception'][0]['message']);
        $this->assertEquals(Response::HTTP_FORBIDDEN, $content['error']['code']);

        /** @var User $user */
        $user = $this->tokenAuth->getUser(['token' => $token], $this->userProvider);

        $this->assertTrue($user instanceof User);

        $user->addRole(self::ROLE);
        $this->em->flush();

        $this->client->request(Request::METHOD_GET, $url, [], [], [
            'HTTP_AUTHORIZATION' => $token,
        ]);
        $response = $this->client->getResponse();
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        $this->assertEquals('[' . $this->serializer->serialize($user, 'json') . ']', $response->getContent());
    }

}