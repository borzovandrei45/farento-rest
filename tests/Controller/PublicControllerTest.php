<?php

namespace App\Tests\rest_api\Controller;

use App\Tests\RestTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class PublicControllerTest
 *
 * @package App\Tests\rest_api\Controller
 */
class PublicControllerTest extends RestTestCase
{
    /**
     * @dataProvider emailProvider
     *
     * @param string $email
     * @param string $status
     * @param string $contentActual
     */
    public function testRegistration(string $email, string $status, string $contentActual)
    {
        $url = $this->router->generate('post_reg');

        $this->client->request(Request::METHOD_POST, $url, [
            'registration' => [
                'email' => 'test' . \rand(0,100) . $email,
                ],
            ]);

        $response = $this->client->getResponse();
        $content  = \json_decode($response->getContent(), true);

        $this->assertEquals($status, $response->getStatusCode());
        $this->assertEquals($contentActual, $content);
    }

    /**
     * @return \Generator
     */
    public function emailProvider(): \Generator
    {
        yield 'success' => [
            'email'   => '@email.com',
            'status'  => Response::HTTP_OK,
            'content' => 'ok',
        ];

        yield 'bad' => [
            'email'   => 'eml.ail@',
            'status'  => Response::HTTP_BAD_REQUEST,
            'content' => 'bad',
        ];
    }
}