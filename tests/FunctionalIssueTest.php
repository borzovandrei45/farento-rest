<?php

namespace App\Tests;

use Symfony\Bundle\SwiftmailerBundle\DataCollector\MessageDataCollector;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mime\Email;

class FunctionalIssueTest extends RestTestCase
{

    public function setUp()
    {
        parent::setUp();

        $this->client->enableProfiler();
    }

    public function testFunctional()
    {
        $url = $this->router->generate('post_reg');

        $this->client->request(Request::METHOD_POST, $url, [
            'registration' => [
                'email' => ($username = 'farento') . '@email.ru',
            ],
        ]);

        $response = $this->client->getResponse();
        $content  = \json_decode($response->getContent(), true);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertEquals('ok', $content);

        /** @var MessageDataCollector $collector */
        $collector         = $this->client->getProfile()->getCollector('swiftmailer');
        $collectedMessages = $collector->getMessages();
        $message           = $collectedMessages[0];

        /** @var Email $mail */
        $document = new \DOMDocument();
        $document->loadHTML($message->getBody());

        $randomPassword = $document->getElementById('pass-id')->nodeValue;
        $this->assertNotNull($randomPassword);

        $templateUsername = $document->getElementById('username-id')->nodeValue;
        $this->assertEquals($username, $templateUsername);
    }

}